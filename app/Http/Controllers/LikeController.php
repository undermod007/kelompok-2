<?php

namespace App\Http\Controllers;

use App\Models\Like;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LikeController extends Controller
{
    public function like(Request $request) : RedirectResponse {
        $user = $request->user();

        Like::create([
            'post_id' => $request->id,
            'user_id' => $user->id
        ]);

        return Redirect::back();
    }

    public function unlike(Request $request) : RedirectResponse {
        $user = $request->user();

        $likedPost = Like::where('post_id', $request->id)
        ->where('user_id', $user->id)
        ->first();

        $likedPost->delete();

        return Redirect::back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\SavedPost;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Response;

class SavedPostController extends Controller
{
    public function save(Request $request) : RedirectResponse {
        $user = $request->user();

        SavedPost::create([
            'post_id' => $request->id,
            'user_id' => $user->id
        ]);

        return Redirect::back();
    }

    public function unsave(Request $request) : RedirectResponse {
        $user = $request->user();

        $savedPost = SavedPost::where('post_id', $request->id)
        ->where('user_id', $user->id)
        ->first();

        $savedPost->delete();

        return Redirect::back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Follow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class FollowController extends Controller
{
    public function follow(Request $request) {
        $user = $request->user();

        Follow::create([
            'follower_user_id' => $user->id,
            'following_user_id' => $request->id
        ]);

        return Redirect::back();
    }

    public function unfollow(Request $request) {
        $user = $request->user();

        $data = Follow::where('follower_user_id', $user->id)
        ->where('following_user_id', $request->id)
        ->first();

        $data->delete();

        return Redirect::back();
    }
}

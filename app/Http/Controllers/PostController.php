<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Report;
use App\Models\User;
use Inertia\Inertia;
use App\Models\UploadedFile;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Inertia\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;

class PostController extends Controller
{
    public function index(Request $request) : Response {
        $user = $request->user();

        if ($user->is_admin == true) {
            $users = User::where('is_admin', false)->latest()->get();
            $posts = Post::with('uploadedFiles')->latest()->get();
            $reports = Report::with('user')->latest()->get();

            return Inertia::render('Home', [
                'users' => $users,
                'posts' => $posts,
                'reports' => $reports
            ]);
        } else {
            $followingUsers = $user->followings->pluck('id');

            // dd($followingUsers);

            $suggests = User::whereNotIn('id', $followingUsers)
            ->where('id', '!=', $user->id)
            ->where('is_admin', false)
            ->latest()
            ->limit(10)
            ->get();
            
            $posts = Post::whereIn('user_id', $followingUsers)
            ->orWhere('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->with('user', 'uploadedFiles', 'likedByUsers', 'comments', 'comments.user', 'savedByUsers')
            ->get();

            return Inertia::render('Home', [
                'posts' => $posts,
                'suggests' => $suggests,
            ]);
        }

    }

    public function store(Request $request) : RedirectResponse {
        $user = $request->user();

        $validated =$request->validate([
            'context' => 'required|max:255',
        ]);
        
        $create = Post::create([
            'user_id' => $user->id,
            'context' => $request->context,
        ]);

        foreach ($request->file('attachments') as $key => $file) {
            $type = $file->getClientMimeType();

            $encodedFile = Image::make($file)->encode('webp')->resize('1080', '1080');
            $path = "uploaded_files/{$create->id}/". Str::of($file->hashName())->basename(".{$file->getClientOriginalExtension()}") . '.webp';

            Storage::disk('public')->put($path, $encodedFile);

            UploadedFile::create([
                'post_id' => $create->id,
                'file_type' => $type,
                'path' => $path
            ]);
        }

        return Redirect::route('home');
    }

    public function show(Request $request) : Response {
        return Inertia::render('Post/Detail', [
            'post' => Post::with('user', 'uploadedFiles', 'likedByUsers', 'comments', 'comments.user', 'savedByUsers')->find($request->id)
        ]);
    }

    public function deletePost(Request $request) : RedirectResponse {
        $data = Post::find($request->id);

        foreach ($data->uploadedFiles as $key => $file) {
            Storage::disk('public')->delete($file->path);
            Storage::disk('public')->deleteDirectory("uploaded_files/{$request->id}");
        }

        $data->delete();

        return Redirect::back();
    }
}

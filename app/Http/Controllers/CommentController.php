<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CommentController extends Controller
{
    public function comment(Request $request) : RedirectResponse {
        $user = $request->user();

        $validated =$request->validate([
            'context' => 'required|max:255',
        ]);

        Comment::create([
            'post_id' => $request->id,
            'user_id' => $user->id,
            'context' => $request->context
        ]);

        return Redirect::back();
    }

    public function deleteComment(Request $request) {
        Comment::find($request->id)->delete();

        return Redirect::back();
    }
}

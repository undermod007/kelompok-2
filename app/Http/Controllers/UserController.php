<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Inertia\Response;

class UserController extends Controller
{
    public function index() : Response {
        return Inertia::render('User/Index', [
            'users' => User::where('is_admin', false)->latest()->get()
        ]);
    }

    public function verified(Request $request) : RedirectResponse {
        $user = User::find($request->id);
        $user->update([
            'verified' => true
        ]);

        return Redirect::back();
    }
}

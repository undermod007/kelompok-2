<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class SearchController extends Controller
{
    public function index(Request $request) : Response {
        $results = User::query()
        ->when($request->input('search'), function($query, $search) {
            $query->where('username', 'like', "%{$search}%");
        })
        ->where('is_admin', false)
        ->limit(10)->get();

        return Inertia::render('Search/Index', [
            'results' => $results
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests\ProfileUpdateRequest;
use App\Models\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Inertia\Response;
use Inertia\Inertia;

class ProfileController extends Controller
{
    public function index(Request $request) : Response {
        $user = $request->user();
        $isFollow = $user->isFollowing($request->id);
        
        return Inertia::render('Profile/Index', [
            'user' => User::with('followers', 'followings', 'posts', 'savedPosts', 'posts.uploadedFiles', 'savedPosts.uploadedFiles')->where('id', $request->id)->first(),
            'isFollow' => $isFollow,
        ]);
    }

    public function update(ProfileUpdateRequest $request) : RedirectResponse {
        $request->user()->fill($request->validated());

        $request->user()->save();

        return Redirect::route('profile.index', [
            'id' => $request->user()->id
        ]);
    }

    public function profilePicture(Request $request) : RedirectResponse {
        $user = $request->user();

        $file = $request->file('profile_picture');
        $encodedFile = Image::make($file)->encode('webp')->resize('1080', '1080');
        $path = "profile_picture/$user->id/" .  Str::of($file->hashName())->basename(".{$file->getClientOriginalExtension()}") . '.webp';

        Storage::disk('public')->put($path, $encodedFile);

        $user->profile_picture = $path;
        $user->save();

        return Redirect::route('profile.index', [
            'id' => $request->user()->id
        ]);
    }
}

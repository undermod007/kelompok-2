<?php

namespace App\Http\Controllers;

use App\Models\Report;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Response;
use Inertia\Inertia;

class ReportController extends Controller
{
    public function index() : Response {
        return Inertia::render('Report/Index', [
            'reports' =>Report::with('user')->latest()->get()
        ]);
    }

    public function report(Request $request) : RedirectResponse {
        $user = $request->user();

        $validated =$request->validate([
            'context' => 'required|max:255',
        ]);

        Report::create([
            'post_id' => $request->id,
            'user_id' => $user->id,
            'context' => $request->context
        ]);

        return Redirect::route('home');
    }
}

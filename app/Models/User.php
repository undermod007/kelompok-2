<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasUuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [
        'id'
    ];
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function followers() : BelongsToMany {
        return $this->belongsToMany(User::class, 'follows', 'following_user_id', 'follower_user_id');
    }

    public function followings() : BelongsToMany {
        return $this->belongsToMany(User::class, 'follows', 'follower_user_id', 'following_user_id');
    }

    public function isFollowing($userId)
    {
        return $this->followings->contains('id', $userId);
    }

    public function posts() : HasMany {
        return $this->hasMany(Post::class, 'user_id', 'id');
    }

    public function likedPosts() : BelongsToMany {
        return $this->belongsToMany(Post::class, 'likes', 'user_id', 'post_id');
    }

    public function hasLikedPost($post) {
        return $this->likedPosts->contains($post);
    }

    public function comments() : HasMany {
        return $this->hasMany(Comment::class, 'user_id', 'id');
    }

    public function savedPosts() : BelongsToMany {
        return $this->belongsToMany(Post::class, 'saved_posts', 'user_id', 'post_id');
    }

    public function reports() : HasMany {
        return $this->hasMany(Report::class, 'user_id', 'id');
    }
}

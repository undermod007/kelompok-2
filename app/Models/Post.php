<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Post extends Model
{
    use HasFactory, HasUuids;

    protected $guarded = [
        'id'
    ];

    public function uploadedFiles() : HasMany {
        return $this->hasMany(UploadedFile::class, 'post_id', 'id');
    }

    public function user() : BelongsTo {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function likes() : HasMany {
        return $this->hasMany(Like::class, 'post_id', 'id');
    }

    public function likedByUsers() : BelongsToMany {
        return $this->belongsToMany(User::class, 'likes', 'post_id', 'user_id');
    }

    public function comments() : HasMany {
        return $this->hasMany(Comment::class, 'post_id', 'id');
    }

    public function savedByUsers() : BelongsToMany {
        return $this->belongsToMany(User::class, 'saved_posts', 'post_id', 'user_id');
    }
}

import Modal from "@/Components/Modal";
import SecondaryButton from "@/Components/SecondaryButton";
import DangerButton from "@/Components/DangerButton";
import { router } from "@inertiajs/react";

export default function UnFollow({ show, setConfirmUnfollow, user, ziggy }) {

    const closeModal = () => {
        setConfirmUnfollow(!show)
    }

    const unfollow = () => {
        router.delete(`/unfollow/${user.id}`)
        closeModal()
    }

    return (
        <Modal show={show} onClose={!show}>
            <div className="p-6 min-h-72">
                <div className="flex-grow">
                    <div className="m-auto w-32 text-center">
                        <img src={`${ziggy.url}/storage/${user.profile_picture}`} alt={`${user.username} Profile Picture`} id="profile-picture" className="rounded-full aspect-square mb-4" />
                        <span id="username" className="text-white text-xl">{user.username}</span>            
                    </div>
                </div>
                <div className="mt-6 flex justify-end gap-x-2">
                    <SecondaryButton onClick={closeModal}>Cancel</SecondaryButton>
                    <DangerButton onClick={unfollow}>Unfollow</DangerButton>
                </div>
            </div>
        </Modal>
    )
}
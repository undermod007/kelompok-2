import React, { useState } from 'react'
import Modal from './Modal'
import { useForm } from '@inertiajs/react';
import InputLabel from './InputLabel';
import InputError from './InputError';
import SecondaryButton from './SecondaryButton';

export default function ReportForm({ isShow, setIsShow, id }) {

    const { data, setData, post, processing, errors, reset } = useForm({
        id: id,
        context: '',
    });

    const [isSuccess, setIsSuccess] = useState(false)

    const close = () => {
        setIsShow(!isShow);
    }

    const submit = (e) => {
        e.preventDefault()
        
        post(route('report'), {
            preserveScroll: true,
            onSuccess: () => {
                close(),
                setIsSuccess(true)
            }
        })
    }

    return (
        <>
            <Modal show={isShow} onClose={close}>
                <form onSubmit={submit} className='p-6'>
                    <div>
                        <InputLabel htmlFor="context" value="Report" />

                        <textarea
                            name="context"
                            id="context"
                            cols="30"
                            rows="10"
                            value={data.context}
                            className="mt-1 block w-full border-gray-700 bg-gray-900 text-gray-300 focus:border-indigo-600 focus:ring-indigo-600 rounded-md shadow-sm"
                            autoComplete="context"
                            onChange={(e) => setData('context', e.target.value)}
                            required
                        ></textarea>

                        <InputError message={errors.context} className="mt-2" />
                    </div>
                    <div className="mt-6 flex justify-end gap-x-2">
                        <SecondaryButton onClick={close}>Cancel</SecondaryButton>
                        <button
                            className='inline-flex items-center px-4 py-2 bg-sky-600 border border-gray-500 rounded-md font-semibold text-xs text-gray-300 uppercase tracking-widest shadow-sm hover:bg-sky-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-gray-800 disabled:opacity-25 transition ease-in-out duration-150'
                            disabled={processing}
                        >
                            Upload
                        </button>
                    </div>
                </form>
            </Modal>
            <Modal show={isSuccess} onClose={() => setIsSuccess(false)}>
                <div className="p-6">
                    <span>Your report will be followed up by Admin</span>
                    <span>Thank you for yoour support.</span>
                </div> 
            </Modal>
        </>
    )
}

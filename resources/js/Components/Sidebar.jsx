import Home from "@/Icons/Home";
import PostModal from "@/Components/PostModal";
import Post from "@/Icons/Post";
import Search from "@/Icons/Search";
import { Link } from "@inertiajs/react";
import { useState } from "react";
import Report from "@/Icons/Report";
import Users from "@/Icons/Users";

export default function Sidebar({ user, appName, ziggy }) {

    const [isShow, setIsShow] = useState(false)

    const openModal = () => {
        setIsShow(true)
    }

    return (
        <>
            <aside className="static h-screen w-96 p-8 overflow-y-scroll no-scrollbar border-r border-gray-800 flex flex-col justify-between">
                <div className="flex flex-col justify-between">
                    <div className="flex justify-start mt-4">
                        <Link href="/" className="text-2xl font-bold text-white hover:bg-clip-text hover:text-transparent hover:bg-gradient-to-r from-cyan-500 to-blue-500 transition ease-in-out duration-300">
                            {appName}
                        </Link>
                    </div>
                    <ul className="mt-5 flex flex-col gap-y-2">
                        {
                            user.is_admin ? (
                                <>
                                    <li className="p-2 hover:bg-gray-800 rounded-lg">
                                        <Link href="/reports" className="flex items-center gap-x-4">
                                            <Report />
                                            <span>Report</span>
                                        </Link>
                                    </li>
                                    <li className="p-2 hover:bg-gray-800 rounded-lg">
                                        <Link href="/users" className="flex items-center gap-x-4">
                                            <Users />
                                            <span>User</span>
                                        </Link>
                                    </li>
                                </>
                            ) : (
                                <>
                                    <li className="p-2 hover:bg-gray-800 rounded-lg">
                                        <Link href="/" className="flex items-center gap-x-4">
                                            <Home />
                                            <span>Home</span>
                                        </Link>
                                    </li>
                                    <li className="p-2 hover:bg-gray-800 rounded-lg">
                                        <Link href="/search" className="flex items-center gap-x-4">
                                            <Search />
                                            <span>Search</span>
                                        </Link>
                                    </li>
                                    <li className="p-2 hover:bg-gray-800 rounded-lg">
                                        <button onClick={openModal} className="flex items-center gap-x-4">
                                            <Post />
                                            <span>Create</span>
                                        </button>
                                    </li>
                                    <li className="p-2 hover:bg-gray-800 rounded-lg">
                                        <Link href={`/profile/${user.id}`} className="flex items-center gap-x-4">
                                            <img src={`${ziggy.url}/storage/${user.profile_picture}`} alt={`${user.username} Profile Picture`} id="profile-picture" className="w-6 h-6 rounded-full ring-2 ring-white" />
                                            <span>Profile</span>
                                        </Link>
                                    </li>
                                </>
                            )
                        }
                    </ul>
                </div>
                <Link href={route('logout')} method="post" as="button" className="my-btn-danger">
                    Logout
                </Link>
            </aside>
            <PostModal isShow={isShow} setIsShow={setIsShow} />
        </>
    )
}
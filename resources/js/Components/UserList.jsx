import React from 'react'
import Modal from './Modal'
import { Link } from '@inertiajs/react';

export default function UserList({datas, show, setIsShow, ziggy}) {
    const closeModal = () => {
        setIsShow(!show)
    }
    
    return (
        <Modal show={show} onClose={closeModal}>
            <div className='p-6 flex flex-col divide-y divide-gray-600'>
                {
                    datas.map((item, i) => (
                        <div className="p-2 flex justify-between items-center" key={i}>
                            <div className='flex items-center'>
                                <img
                                    src={`${ziggy.url}/storage/${item.profile_picture}`}
                                    alt={`${item.username} Profile Picture`}
                                    id="profile-picture" className="rounded-full aspect-square w-10 h-10 mr-4"
                                />
                                <h1 className='font-bold'>{item.username}</h1> 
                            </div>
                            <Link href={`/profile/${item.id}`} className='text-sky-500 text-sm font-bold'>
                                Profile
                            </Link>
                        </div>
                    ))
                }
            </div>
        </Modal>
    )
}

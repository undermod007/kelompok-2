import Comment from '@/Icons/Comment';
import ElipsisHorizontal from '@/Icons/ElipsisHorizontal';
import Like from '@/Icons/Like';
import Save from '@/Icons/Save';
import Share from '@/Icons/Share';
import { Link } from '@inertiajs/react';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
 
dayjs.extend(relativeTime);
import React, { useState } from 'react'
import UserList from './UserList';
import CommentList from './CommentList';
import CopyToClipboard from 'react-copy-to-clipboard';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination } from 'swiper/modules'
import Dropdown from './Dropdown';
import DangerButton from './DangerButton';
import ReportForm from './ReportForm';
import Verified from '@/Icons/Verified';

export default function PostCard({ auth, post, user, uploadedFiles, ziggy }) {
    const isLiked = post.liked_by_users.filter((item) => {
        return item.pivot.user_id == auth.id
    });

    const isSaved = post.saved_by_users.filter((item) => {
        return item.pivot.user_id == auth.id
    })

    const [isShow, setIsShow] = useState(false)
    const [passData, setPassData] = useState([])
    const [isOpen, setIsOpen] = useState(false)
    const [contextFull, setContextFull] = useState(100)
    const [openReport, setOpenReport] = useState(false)

    const openUserList = () => {
        setIsShow(!isShow)
        setPassData(post.liked_by_users)
    }

    const openCommentList = () => {
        setIsOpen(!isOpen)
    }

    const share = () => {
        alert('Copied to clipboard')
    }

    return (
        <div className='flex flex-col gap-y-2 h-fit my-6'>
            <div className='flex flex-row justify-between items-center'>
                <div className='p-2 flex items-center gap-x-2 overflow-hidden relative'>
                    <img
                        src={`${ziggy.url}/storage/${user.profile_picture}`}
                        alt={`${user.username} Profile Picture`}
                        className="w-16 h-16 absolute -left-4 rounded-full"
                    />
                    <Link href={`/profile/${user.id}`} className="text-white text-md font-bold py-2 pl-16 hover:underline">
                        {user.username}
                    </Link>
                    {
                        user.verified ? <Verified /> : null
                    }
                    <div>•</div>
                    <small className='text-sm text-gray-300'>{dayjs(post.created_at).fromNow()}</small>  
                </div>
                <Dropdown>
                    <Dropdown.Trigger>
                        <button className='pr-2'>
                            <ElipsisHorizontal />
                        </button>
                    </Dropdown.Trigger>
                    <Dropdown.Content>
                        {
                            post.user_id == auth.id ? (
                                <Dropdown.Link href={`/post/delete/${post.id}`} method='delete' as='button'>
                                    <button className='p-2 bg-red-600 rounded-lg text-center text-white font-bold uppercase w-full'>Delete</button>
                                </Dropdown.Link>
                            ) : (
                                <div className="py-2 px-4">
                                    <button onClick={() => setOpenReport(!openReport)} className='p-2 bg-red-600 rounded-lg text-center text-white font-bold uppercase w-full'>Report</button>
                                </div>
                            )
                        }
                    </Dropdown.Content>
                </Dropdown>
            </div>
            {
                uploadedFiles.length > 1 ? (
                    <div>
                        <Swiper
                            pagination={{
                                dynamicBullets: true
                            }}
                            modules={[Pagination]}
                            spaceBetween={10}
                            slidesPerView={1}
                        >
                            {
                                uploadedFiles.map((item, i) => (
                                    item.file_type == "image/jpeg" || item.file_type == "images/png" || item.file_type == "image/png" ? (
                                            <SwiperSlide key={i}>
                                                <img
                                                    src={`${ziggy.url}/storage/${item.path}`}
                                                    alt={`Posted by ${user.username}`}
                                                    className='rounded-lg aspect-auto'
                                                />
                                            </SwiperSlide>
                                    ) : (
                                        null
                                    )
                                ))
                            }
                        </Swiper>
                    </div>
                ) : (
                    uploadedFiles.map((item, i) => (
                        item.file_type == "image/jpeg" || item.file_type == "images/png" || item.file_type == "image/png" ? (
                            <img
                                src={`${ziggy.url}/storage/${item.path}`}
                                alt={`Posted by ${user.username}`}
                                className='rounded-lg aspect-auto'
                                key={i}
                            />
                        ) : (
                            null
                        )
                    ))
                )
            }
            <div className='flex flex-row justify-between'>
                <div className='p-2 flex items-center gap-x-4'>
                    <Link href={post.liked_by_users.length != 0 && isLiked.length != 0 ? '/unlike' : '/like'} data={{id: post.id}} method='post' as='button'>
                        <Like isLiked={post.liked_by_users.length != 0 && isLiked.length != 0} />
                    </Link>
                    <button onClick={openCommentList}>
                        <Comment />
                    </button>
                    <CopyToClipboard
                        text={ziggy.url + '/post/detail/' + post.id}
                        onCopy={share}
                    >
                        <button>
                            <Share />
                        </button>
                    </CopyToClipboard>
                </div>
                <Link href={post.saved_by_users.length != 0 && isSaved.length != 0 ? '/unsave' : '/save'} data={{id: post.id}} method='post' as='button'>
                    <Save isSaved={post.saved_by_users.length != 0 && isSaved.length != 0} />
                </Link>
            </div>
            <div className="px-2">
                <button onClick={openUserList} className='hover:underline'>{post.liked_by_users.length} likes</button>
                <div>
                    <span className='font-bold'>{user.username}</span>
                    {
                        post.context.length >= 100 ? (
                            post.context.length != contextFull ? (
                                <>
                                    <p>{' ' + post.context.substring(0, contextFull) + '...'}</p>
                                    <button onClick={() => setContextFull(post.context.length)} className='font-bold text-sm mt-4 text-gray-400 hover:underline'>More</button>
                                </>
                            ) : (
                                <>
                                    <p>{' ' + post.context}</p>
                                    <button onClick={() => setContextFull(100)} className='font-bold text-sm mt-4 text-gray-400 hover:underline'>Less</button>
                                </>
                            )
                        ) : (
                            <p>{' ' + post.context}</p>
                        )
                    }
                </div>
            </div>
            <div className="px-2 mt-6">
                {
                    post.comments.length != 0 && (
                        <button
                            onClick={openCommentList}
                            className='text-sm text-gray-400 hover:underline mb-2'
                        >
                            View all {post.comments.length} comments
                        </button>
                    )
                }
            </div>
            <CommentList show={isOpen} setIsShow={setIsOpen} datas={post} ziggy={ziggy} auth={auth} />
            <UserList show={isShow} datas={passData} setIsShow={setIsShow} ziggy={ziggy}/>
            <ReportForm isShow={openReport} setIsShow={setOpenReport} id={post.id} />
        </div>
    )
}

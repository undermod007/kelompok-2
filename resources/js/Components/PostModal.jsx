import React from 'react'
import Modal from './Modal'
import SecondaryButton from './SecondaryButton'
import InputLabel from './InputLabel'
import InputError from './InputError'
import { useForm } from '@inertiajs/react'

function PostModal({ isShow, setIsShow }) {
    const { data, setData, post, processing, errors, reset } = useForm({
        context: '',
        attachments: [],
    });

    const closeModal = () => {
        setIsShow(!isShow)
    }

    const storePost = (e) => {
        e.preventDefault()

        post(route('post.store'), {
            preserveScroll: true,
            onSuccess: () => closeModal(),
        })
    }

    return (
        <Modal show={isShow} onClose={closeModal}>
            <form onSubmit={storePost} className='p-6'>
                <div>
                    <InputLabel htmlFor="context" value="Caption" />

                    <textarea
                        name="context"
                        id="context"
                        cols="30"
                        rows="10"
                        value={data.context}
                        className="mt-1 block w-full border-gray-700 bg-gray-900 text-gray-300 focus:border-indigo-600 focus:ring-indigo-600 rounded-md shadow-sm"
                        autoComplete="context"
                        isFocused={true}
                        onChange={(e) => setData('context', e.target.value)}
                        required
                    ></textarea>

                    <InputError message={errors.context} className="mt-2" />
                </div>
                <div className='mt-4'>
                    <input
                        type="file"
                        name="attachments"
                        id="attachments"
                        className='border-gray-700 bg-gray-900 text-gray-300 focus:border-indigo-600 focus:ring-indigo-600 rounded-md shadow-sm'
                        onChange={(e) => setData('attachments', e.target.files)}
                        multiple
                        required
                        accept='image/*'
                    />

                    <InputError message={errors.attachments} className="mt-2" />
                </div>
                <div className="mt-6 flex justify-end gap-x-2">
                    <SecondaryButton onClick={closeModal}>Cancel</SecondaryButton>
                    <button
                        className='inline-flex items-center px-4 py-2 bg-sky-600 border border-gray-500 rounded-md font-semibold text-xs text-gray-300 uppercase tracking-widest shadow-sm hover:bg-sky-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-gray-800 disabled:opacity-25 transition ease-in-out duration-150'
                        disabled={processing}
                    >
                        Upload
                    </button>
                </div>
            </form>
        </Modal>
    )
}

export default PostModal
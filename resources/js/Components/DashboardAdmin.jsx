import Post from '@/Icons/Post'
import Report from '@/Icons/Report'
import Users from '@/Icons/Users'
import React from 'react'

export default function DashboardAdmin({ users, ziggy, posts, reports }) {
    return (
        <div className='p-6 w-full'>
            <h1 className='font-bold text-xl'>Dashboard Admin</h1>
            <div className="flex flex-row justify-between gap-4 max-w-full mt-6">
                <div className="border border-gray-800 hover:border-gray-400 rounded-xl p-2 flex flex-col gap-y-2 items-center justify-around flex-grow h-64">
                    <Users />
                    <div className='text-center'>
                        <h1 className='font-bold text-2xl'>{users.length}</h1>
                        <h2>Users</h2>
                    </div>
                    <div className="flex flex-row">
                        {
                            users.slice(0, 4).map((item, i) => (
                                <img
                                    src={`${ziggy.url}/storage/${item.profile_picture}`}
                                    alt={`${item.username} Profile Picture`}
                                    id="profile-picture" className="rounded-full aspect-square w-7 h-7"
                                    key={i}
                                />
                            ))
                        }
                    </div>
                </div>
                <div className="border border-gray-800 hover:border-gray-400 rounded-xl p-2 flex flex-col gap-y-2 items-center justify-around flex-grow h-64">
                    <Post />
                    <div className='text-center'>
                        <h1 className='font-bold text-2xl'>{posts.length}</h1>
                        <h2>Posts</h2>
                    </div>
                    <div className="flex flex-row">
                        {
                            posts.slice(0, 5).map((item, i) => (
                                <img
                                    src={`${ziggy.url}/storage/${item.uploaded_files[0].path}`}
                                    alt={`Posted by ${item.id}`}
                                    className='rounded-lg aspect-auto w-7 h-7'
                                    key={i}
                                />
                            ))
                        }
                    </div>
                </div>
                <div className="border border-gray-800 hover:border-gray-400 rounded-xl p-2 flex flex-col gap-y-2 items-center justify-around flex-grow h-64">
                    <Report />
                    <div className='text-center'>
                        <h1 className='font-bold text-2xl'>{reports.length}</h1>
                        <h2>Reports</h2>
                    </div>
                    <div className="flex flex-row">
                        {
                            reports.slice(0, 5).map((item, i) => (
                                <img
                                    src={`${ziggy.url}/storage/${item.user.profile_picture}`}
                                    alt={`${item.user.username} Profile Picture`}
                                    id="profile-picture" className="rounded-full aspect-square w-7 h-7"
                                    key={i}
                                />
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

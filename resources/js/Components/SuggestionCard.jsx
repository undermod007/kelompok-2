import React from 'react'
import { Link } from '@inertiajs/react';
import Verified from '@/Icons/Verified';

export default function SuggestionCard({ user, ziggy, suggests }) {
    return (
        <div className='w-96 ml-16 mt-4'>
            <div className="flex flex-col">
                <div className='flex-grow'>
                    <div className="p-2 flex justify-between items-center">
                        <div className='flex'>
                            <img
                                src={`${ziggy.url}/storage/${user.profile_picture}`}
                                alt={`${user.username} Profile Picture`}
                                id="profile-picture" className="rounded-full aspect-square w-10 h-10 mr-4"
                            />
                            <div className="flex flex-col justify-center">
                                <div className='flex items-center gap-x-2'>
                                    <h1 className='font-bold leading-none'>{user.username}</h1> 
                                    {
                                        user.verified ? <Verified /> : null
                                    }
                                </div>
                                <h2 className='leading-none'>{user.name}</h2> 
                            </div>
                        </div>
                        <Link href={`/profile/${user.id}`} className='text-sky-500 text-sm font-bold'>
                            Profile
                        </Link>
                    </div>
                    <div className="p-2 flex flex-col gap-y-2">
                        <h1 className='font-bold leading-none text-gray-400 text-sm'>Suggested for you</h1>
                    </div>
                    <div className='flex flex-col gap-y-2'>
                        {
                            suggests.map((item, i) => (
                                <div className="p-2 flex justify-between items-center" key={i}>
                                    <div className='flex'>
                                        <img
                                            src={`${ziggy.url}/storage/${item.profile_picture}`}
                                            alt={`${item.username} Profile Picture`}
                                            id="profile-picture" className="rounded-full aspect-square w-10 h-10 mr-4"
                                        />
                                        <div className="flex flex-row items-center gap-x-2">
                                            <h1 className='font-bold leading-none'>{item.username}</h1> 
                                            {
                                                item.verified ? <Verified /> : null
                                            }
                                        </div>
                                    </div>
                                    <Link href={`/profile/${item.id}`} className='text-sky-500 text-sm font-bold'>
                                        Profile
                                    </Link>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

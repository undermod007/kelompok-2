import { Link } from '@inertiajs/react';
import React from 'react'

export default function GridPost({ datas, ziggy }) {
    console.log(datas);
    return (
        datas.map((item, i) => (
            <Link href={`/post/detail/${item.id}`} method='get' as='button' key={i}>
                <img
                    src={`${ziggy.url}/storage/${item.uploaded_files[0].path}`}
                    alt={item.id}
                    className='rounded-md aspect-square bg-cover bg-center grayscale blur-sm hover:grayscale-0 hover:blur-none transition ease-in-out delay-150 duration-300'
                    key={i}
                />
            </Link>
        ))
    )
}

import React, { useState } from 'react'
import TextInput from './TextInput'
import { Link, useForm } from '@inertiajs/react';
import InputLabel from './InputLabel';
import Modal from './Modal';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination } from 'swiper/modules'
import InputError from './InputError';
 
dayjs.extend(relativeTime);

export default function CommentList({ show, datas, ziggy, auth, setIsShow }) {

	const { data, setData, post, processing, errors, reset } = useForm({
        id: datas.id,
        context: '',
    });

	const submit = (e) => {
        e.preventDefault()

        post(route('comment'), {
            preserveScroll: true,
            onFinish: () => reset(),
        })
    }

	const close = () => {
		setIsShow(!show)
	}

	return (
		<div className="px-2">
			<InputLabel htmlFor="context" value="Add comment" />

			<textarea
				id="context"
				name="context"
				value={data.context}
				className="border-gray-700 bg-gray-900 text-gray-300 focus:border-indigo-600 focus:ring-indigo-600 rounded-md shadow-sm mt-1 block w-full"
				autoComplete="context"	
				onChange={(e) => setData('context', e.target.value)}
				required
			></textarea>

			<InputError message={errors.context} className="mt-2" />
			<button
				onClick={submit}
				disabled={processing}
				className="px-2 py-1 hover:bg-sky-500 text-sky-500 hover:text-white rounded-md text-sm font-bold mt-4 transition-all"
			>
				Post
			</button>

			<Modal show={show} onClose={close} maxWidth={'3xl'}>
				<div className='flex flex-row m-auto'>
					<div className='flex-grow w-1/2'>
						{
							datas.uploaded_files.length > 1 ? (
								<Swiper
									pagination={{
										dynamicBullets: true
									}}
									modules={[Pagination]}
								>
									{
										datas.uploaded_files.map((item, i) => (
											item.file_type == "image/jpeg" || item.file_type == "images/png" || item.file_type == "image/png" ? (
												<SwiperSlide key={i}>
													<img
														src={`${ziggy.url}/storage/${item.path}`}
														alt={`Posted by ${datas.user.username}`}
														className='rounded-lg w-full h-full aspect-square'
													/>
												</SwiperSlide>
											) : (
												null
											)
										))
									}
								</Swiper>
							) : (
								datas.uploaded_files.map((item, i) => (
									item.file_type == "image/jpeg" || item.file_type == "images/png" || item.file_type == "image/png" ? (
										<img
											src={`${ziggy.url}/storage/${item.path}`}
											alt={`Posted by ${datas.user.username}`}
											className='rounded-lg w-full h-full aspect-square'
											key={i}
										/>
									) : (
										null
									)
								))
							)
						}
					</div>
					<div className='p-6 flex flex-col divide-y divide-gray-600 h-96 w-1/2'>
						<div className="overflow-y-scroll no-scrollbar">
							<div className="p-2 flex flex-col justify-between items-start border-b border-gray-400">
								<div className='flex items-center'>
									<img
										src={`${ziggy.url}/storage/${datas.user.profile_picture}`}
										alt={`${datas.user.username} Profile Picture`}
										id="profile-picture" className="rounded-full aspect-square w-10 h-10 mr-4"
									/>
									<Link href={`profile/${datas.user.id}`} className='font-bold hover:underline'>
										{datas.user.username}
									</Link>
								</div>
								<span className='mt-2 text-sm font-normal hover:no-underline'>{datas.context}</span>
							</div>
							{
								datas.comments.map((item, i) => (
									<div className="p-2 flex flex-col justify-between items-start" key={i}>
										<div className='flex items-center'>
											<img
												src={`${ziggy.url}/storage/${item.user.profile_picture}`}
												alt={`${item.user.username} Profile Picture`}
												id="profile-picture" className="rounded-full aspect-square w-10 h-10 mr-4"
											/>
											<Link href={`profile/${item.user.id}`} className='font-bold hover:underline'>
												{item.user.username}
											</Link>
											<span className='ml-2 text-sm font-normal hover:no-underline'>{item.context}</span>
										</div>
										<div className="flex gap-x-2 items-center mt-4">
											<small className="text-gray-400">{dayjs(item.created_at).fromNow()}</small>
											{
												auth.id == item.user.id && (
													<Link
														href="/delete/comment"
														data={{ id: item.id }}
														method='delete'
														as='button'
														className='text-sm text-red-500 px-2 py-1 hover:bg-red-500 hover:text-white rounded-md font-bold transition-all'
													>
														Delete
													</Link>
												)
											}
										</div>
									</div>
								))
							}
						</div>
					</div>
				</div>
			</Modal>
		</div>
	)
}

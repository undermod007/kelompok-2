import Sidebar from "@/Components/Sidebar";
import { Head } from "@inertiajs/react";

export default function Base({ user, title, appName, ziggy, children, users }) {
    return (
        <>
            <Head title={title} />
            <div className="flex flex-row h-screen bg-gray-950 max-w-full">
                <Sidebar user={user} appName={appName} ziggy={ziggy} users={users} />

                {children}
            </div>
        </>
    )
}
import { Link, router, useForm } from '@inertiajs/react';
import TextInput from '@/Components/TextInput';
import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import SecondaryButton from "@/Components/SecondaryButton";
import Modal from "@/Components/Modal";
import Base from "@/Layouts/Base";
import { useState, useRef, useEffect } from "react";
import UnFollow from '@/Components/UnFollow';
import UserList from '@/Components/UserList';
import GridPost from '@/Components/GridPost';
import Verified from '@/Icons/Verified';
import Setting from '@/Icons/Setting';

export default function Profile({ auth, user, isFollow, ziggy, appName, users }) {
    const inputFile = useRef(null) 

    const [isShow, setIsShow] = useState(false)
    const [openFollowersList, setOpenFollowersList] = useState(false)
    const [openFollowingsList, setOpenFollowingsList] = useState(false)
    const [confirmUnfollow, setConfirmUnfollow] = useState(false)
    const [passData, setPassData] = useState([])
    const [file, setFile] = useState(null)
    const [chooseFilter, setChooseFilter] = useState('posts')

    const { data, setData, post, processing, errors, reset } = useForm({
        name: '',
        username: '',
    });

    const showModal = () => {
        setIsShow(true)
    }

    const closeModal = () => {
        setIsShow(false)
    }

    const openFileDialog = () => {
        inputFile.current.click();
    }

    const submit = (e) => {
        e.preventDefault()

        post(route('profile.update'), {
            preserveScroll: true,
            onSuccess: () => closeModal(),
            onFinish: () => reset(),
        })
    }

    const uploadFile = () => {
        router.post('/profile/picture', {
            _method: 'put',
            profile_picture: file
        })
    }

    const follow = () => {
        router.post(`/follow/${user.id}`)
    }

    const setStateConfirmUnfollow = () => {
        setConfirmUnfollow(!confirmUnfollow)
    }

    const openFollowers = () => {
        setOpenFollowersList(!openFollowersList)
        setPassData(user.followers)
    }
    
    const openFollowings = () => {
        setOpenFollowersList(!openFollowingsList)
        setPassData(user.followings)
    }

    useEffect(() => {
        if (file != null) {
            uploadFile()
        }
    }, [file])

    return (
        <Base user={auth.user} title={`${user.username}`} appName={appName} ziggy={ziggy} users={users}>
            <section className="flex flex-col min-h-screen flex-grow">
                <main className="flex flex-col flex-grow">
                    <div className="mx-auto p-8 w-[900px]">
                        <header className="mb-11 flex items-stretch relative">
                            <div className="mr-8 flex flex-col justify-center relative flex-grow flex-shrink-0">
                                <div className="m-auto w-40">
                                    <img
                                        src={`${ziggy.url}/storage/${user.profile_picture}`}
                                        alt={`${user.username} Profile Picture`}
                                        id="profile-picture"
                                        className="rounded-full aspect-square"
                                        
                                    />
                                </div>
                            </div>
                            <section className="flex flex-col items-stretch gap-y-6 relative flex-grow">
                                <div className="flex items-center relative gap-x-4">
                                    <h1 id="username" className="text-white text-xl">{user.username}</h1>
                                    {
                                        user.verified ? <Verified /> : null
                                    }
                                    {
                                        auth.user.id == user.id ? (
                                            <>
                                                <button onClick={showModal} className="px-2 py-1 bg-gray-800 text-white rounded-md text-sm font-bold">Edit Profile</button>
                                                <button onClick={openFileDialog} className="px-2 py-1 bg-gray-800 text-white rounded-md text-sm font-bold">Change Profile Picture</button>
                                                <input type="file" name="profile_picture" id="profile_picture" className='hidden' ref={inputFile} onChange={(e) => setFile(e.target.files[0])} accept='image/*' />
                                                <Link href="/setting" method='get' as='button'>
                                                    <Setting />
                                                </Link>
                                            </>
                                        ) : (
                                            auth.user.is_admin == 0 ? (
                                                isFollow ? (
                                                    <button onClick={setStateConfirmUnfollow} className="px-2 py-1 bg-gray-800 text-white rounded-md text-sm font-bold">Following</button>
                                                ) : (
                                                    <button onClick={follow} className="px-2 py-1 bg-sky-500 text-white rounded-md text-sm font-bold">Follow</button>
                                                )
                                            ) : (
                                                null
                                            )
                                        )
                                    }
                                </div>
                                <div className="flex items-center relative gap-x-6" id="statistics">
                                    <span>{user.posts.length} Posts</span>
                                    <button onClick={openFollowers}>{user.followers.length} Followers</button>
                                    <button onClick={openFollowings}>{user.followings.length} Following</button>
                                </div>
                                <span id="name" className="text-white font-bold text-sm">{user.name}</span>
                            </section>
                        </header>
                    </div>
                    <div className="mx-auto py-4 w-[900px] border-t border-gray-400">
                        <div className="flex flex-col gap-y-4">
                            {
                                auth.user.id == user.id && (
                                    <div className="flex flex-row justify-center gap-x-4">
                                        <button
                                            className={chooseFilter == 'posts' ? 'text-sm font-bold px-2 py-1 bg-white text-gray-950 rounded-md' : 'text-sm font-bold p-2 text-white'}
                                            onClick={() => setChooseFilter('posts')}
                                        >
                                            Posts
                                        </button>
                                        <button
                                            className={chooseFilter == 'saved' ? 'text-sm font-bold px-2 py-1 bg-white text-gray-950 rounded-md' : 'text-sm font-bold p-2 text-white'}
                                            onClick={() => setChooseFilter('saved')}
                                        >
                                            Saved
                                        </button>
                                    </div>
                                )
                            }
                            {
                                chooseFilter == 'posts' ? (
                                    <div className="grid grid-cols-3 gap-2">
                                        <GridPost datas={user.posts} ziggy={ziggy} />
                                    </div>
                                ) : (
                                    <div className="grid grid-cols-3 gap-2">
                                        <GridPost datas={user.saved_posts} ziggy={ziggy} />
                                    </div>
                                )
                            }
                        </div>
                    </div>
                </main>
            </section>

            <Modal show={isShow} onClose={closeModal}>
                <form onSubmit={submit} className="p-6">
                    <div>
                        <InputLabel htmlFor="name" value="Name" />

                        <TextInput
                            id="name"
                            name="name"
                            value={data.name}
                            className="mt-1 block w-full"
                            autoComplete="name"
                            isFocused={true}
                            onChange={(e) => setData('name', e.target.value)}
                        />

                        <InputError message={errors.name} className="mt-2" />
                    </div>
                    <div className='mt-4'>
                        <InputLabel htmlFor="username" value="Username" />

                        <TextInput
                            id="username"
                            name="username"
                            value={data.username}
                            className="mt-1 block w-full"
                            autoComplete="username"
                            isFocused={true}
                            onChange={(e) => setData('username', e.target.value)}
                        />

                        <InputError message={errors.username} className="mt-2" />
                    </div>
                    <div className="mt-6 flex justify-end gap-x-2">
                        <SecondaryButton onClick={closeModal}>Cancel</SecondaryButton>
                        <button disabled={processing} className='px-4 py-2 bg-cyan-500 rounded-md'>Submit</button>
                    </div>
                </form>
            </Modal>

            <UnFollow show={confirmUnfollow} user={user} setConfirmUnfollow={setConfirmUnfollow} ziggy={ziggy} />

            <UserList show={openFollowersList} datas={passData} setIsShow={openFollowersList ? setOpenFollowersList : setOpenFollowingsList} ziggy={ziggy} />
        </Base>
    )
}
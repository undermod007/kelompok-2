import PostCard from '@/Components/PostCard';
import Base from '@/Layouts/Base';
import React from 'react'

export default function Detail(props) {
    console.log(props);
    return (
        <Base user={props.auth.user} title={`${props.post.user.username}`} appName={props.appName} ziggy={props.ziggy}>
            <div className="w-full ml-auto overflow-y-auto overflow-x-hidden">
                <section className="min-h-screen flex flex-col">
                    <main className="flex flex-col flex-grow">
                        <div className="flex flex-row justify-center w-full pt-6">
                            <div className='max-w-lg flex-grow'>
                                <PostCard
                                    post={props.post}
                                    auth={props.auth.user}
                                    user={props.post.user}
                                    uploadedFiles={props.post.uploaded_files}
                                    ziggy={props.ziggy}
                                />
                            </div>
                        </div>
                    </main>
                </section>
            </div>
        </Base>
    )
}

import Base from '@/Layouts/Base';
import { Link } from '@inertiajs/react';
import React, { useMemo } from 'react'
import DataTable from 'react-data-table-component';

export default function Index(props) {
    console.log(props.reports);

    const columns = useMemo(() => [
        {
            name: 'Reporter',
            selector: row => row.user.username,
            cell: (row) => <Link href={'/profile/' + row.user.id} className='hover:underline'>{row.user.username}</Link>,
            sortable: true,
        },
        {
            name: 'Description',
            selector: row => row.context,
            sortable: true,
        },
        {
            name: 'Detail Post',
            cell: (row) => <Link href={props.ziggy.url + '/post/detail/' + row.post_id} className='px-4 py-2 rounded-md bg-sky-500 text-white'>Detail Post</Link>,
            ignoreRowClick: true,
            allowOverflow: true,
            button: false,
        },
        {
            name: 'Delete Post',
            cell: (row) => <Link href={props.ziggy.url + '/post/delete/' + row.post_id} method='delete' as='button' className='px-4 py-2 rounded-md bg-red-500 text-white'>Delete Post</Link>,
            ignoreRowClick: true,
            allowOverflow: true,
            button: true,
        }
    ]);
    
    return (
        <Base user={props.auth.user} title={"Home"} appName={props.appName} ziggy={props.ziggy}>
            <div className="w-full p-4">
                <DataTable
                    title="Data Reports"
                    columns={columns}
                    data={props.reports}
                    theme="dark"
                    pagination 
                />
            </div>
        </Base>
    )
}

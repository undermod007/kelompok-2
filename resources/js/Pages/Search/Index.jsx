import InputLabel from '@/Components/InputLabel'
import TextInput from '@/Components/TextInput'
import Base from '@/Layouts/Base'
import { Link, router } from '@inertiajs/react';
import React, { useState } from 'react'

export default function Index(props) {
    console.log(props.results);
    const [username, setUsername] = useState('')

    const search = (keyword) => {
        setUsername(keyword)
        router.get('/search', { search: keyword }, {
            preserveState: true
        })
    }

    return (
        <Base user={props.auth.user} title="Search" appName={props.appName} ziggy={props.ziggy}>
            <div className="w-full p-6">
                <InputLabel htmlFor="keyword" value="Search by username" />

                <TextInput
                    id="name"
                    name="name"
                    value={username}
                    className="mt-1 block w-full"
                    autoComplete="name"
                    isFocused={true}
                    onChange={(e) => search(e.target.value)}
                />

                <div className="flex flex-col mt-6">
                    {
                        username != '' && (
                            props.results.map((item, i) => (
                                <div className="p-2 flex justify-between items-center" key={i}>
                                    <div className='flex'>
                                        <img
                                            src={`${props.ziggy.url}/storage/${item.profile_picture}`}
                                            alt={`${item.username} Profile Picture`}
                                            id="profile-picture" className="rounded-full aspect-square w-10 h-10 mr-4"
                                        />
                                        <div className="flex flex-col justify-center">
                                            <h1 className='font-bold leading-none'>{item.username}</h1> 
                                            <h2 className='leading-none'>{item.name}</h2> 
                                        </div>
                                    </div>
                                    <Link href={`/profile/${item.id}`} className='text-sky-500 text-sm font-bold'>
                                        Profile
                                    </Link>
                                </div>
                            ))
                        )
                    }
                </div>
            </div>
        </Base>
    )
}

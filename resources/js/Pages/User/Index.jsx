import React, { useMemo } from 'react'
import Base from '@/Layouts/Base'
import DataTable from 'react-data-table-component'
import Verified from '@/Icons/Verified'
import NotVerified from '@/Icons/NotVerified'
import { Link } from '@inertiajs/react'

export default function Index(props) {
    const columns = useMemo(() => [
        {
            name: "Name",
            selector: row => row.name,
            sortable: true,
        },
        {
            name: "Username",
            selector: row => row.username,
            sortable: true,
        },
        {
            name: "Verified",
            selector: row => row.username,
            cell: (row) => row.verified ? <Verified /> : <NotVerified />,
            sortable: true,
        },
        {
            name: "Links",
            cell: (row) => <Link href={'/profile/' + row.id} className='px-4 py-2 rounded-md bg-sky-500 text-white'>Profile</Link>,
            ignoreRowClick: true,
            allowOverflow: true,
            button: false,
        },
        {
            name: "Verified",
            cell: (row) => <Link href={'/users/verified'} data={{ id: row.id }} method='post' as='button' className='px-4 py-2 rounded-md bg-sky-500 text-white'>Verified</Link>,
            ignoreRowClick: true,
            allowOverflow: true,
            button: false,
        }
    ])

    return (
        <Base user={props.auth.user} title={"Home"} appName={props.appName} ziggy={props.ziggy}>
            <div className="w-full p-4">
                <DataTable
                    title="Data Users"
                    columns={columns}
                    data={props.users}
                    theme="dark"
                    pagination 
                />
            </div>
        </Base>
    )
}

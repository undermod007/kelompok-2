import DashboardAdmin from "@/Components/DashboardAdmin";
import PostCard from "@/Components/PostCard";
import SuggestionCard from "@/Components/SuggestionCard";
import Base from "@/Layouts/Base";

export default function Home(props) {
    console.log(props);
    return (
        <Base user={props.auth.user} title={"Home"} appName={props.appName} ziggy={props.ziggy} users={props.users}>
            {
                props.auth.user.is_admin ? (
                    <DashboardAdmin
                        users={props.users}
                        ziggy={props.ziggy}
                        posts={props.posts}
                        reports={props.reports}
                    />
                ) : (
                    <div className="w-full ml-auto overflow-y-auto overflow-x-hidden">
                        <section className="min-h-screen flex flex-col">
                            <main className="flex flex-col flex-grow">
                                <div className="flex flex-row justify-center w-full pt-6">
                                    <div className='max-w-lg'>
                                        {
                                            props.posts.length == 0 ? (
                                                <div className='flex flex-row gap-y-2 h-screen mt-4 items-center max-w-md'>
                                                    <div className="flex-grow text-center">
                                                        <h1 className="font-bold text-3xl mb-4">You have nothing to see here</h1>
                                                        <small className="text-md">Try to follow some account to see what happen or create some post.</small>
                                                    </div>
                                                </div>
                                            ) : (
                                                props.posts.map((item, i) => (
                                                    <PostCard
                                                        post={item}
                                                        auth={props.auth.user}
                                                        user={item.user}
                                                        uploadedFiles={item.uploaded_files}
                                                        ziggy={props.ziggy}
                                                        key={i}
                                                    />
                                                ))
                                            )
                                        }
                                    </div>
                                    <SuggestionCard user={props.auth.user} ziggy={props.ziggy} suggests={props.suggests} />
                                </div>
                            </main>
                        </section>
                    </div>
                )
            }
        </Base>
    )
}
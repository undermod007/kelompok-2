<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\FollowController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SavedPostController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function() {
    Route::get('/', [PostController::class, 'index'])->name('home');

    Route::get('/profile/{id}', [ProfileController::class, 'index'])->name('profile.index');
    Route::post('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::put('/profile/picture', [ProfileController::class, 'profilePicture'])->name('profile.picture');

    Route::post('/follow/{id}', [FollowController::class, 'follow'])->name('follow');
    Route::delete('/unfollow/{id}', [FollowController::class, 'unfollow'])->name('unfollow');

    Route::post('/post', [PostController::class, 'store'])->name('post.store');
    Route::get('/post/detail/{id}', [PostController::class, 'show'])->name('post.show');
    Route::delete('/post/delete/{id}', [PostController::class, 'deletePost'])->name('post.delete');

    Route::post('/like', [LikeController::class, 'like'])->name('like');
    Route::post('/unlike', [LikeController::class, 'unlike'])->name('unlike');

    Route::post('/comment', [CommentController::class, 'comment'])->name('comment');
    Route::delete('/delete/comment', [CommentController::class, 'deleteComment'])->name('delete.comment');

    Route::post('/save', [SavedPostController::class, 'save'])->name('save');
    Route::post('/unsave', [SavedPostController::class, 'unsave'])->name('unsave');

    Route::get('/reports', [ReportController::class, 'index'])->name('report.get');
    Route::post('/post/report', [ReportController::class, 'report'])->name('report');

    Route::get('/users', [UserController::class, 'index'])->name('users');
    Route::post('/users/verified', [UserController::class, 'verified'])->name('users.verified');

    Route::get('/search', [SearchController::class, 'index'])->name('index.search');
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/setting', [SettingController::class, 'edit'])->name('setting.edit');
    Route::patch('/setting', [SettingController::class, 'update'])->name('setting.update');
    Route::delete('/setting', [SettingController::class, 'destroy'])->name('setting.destroy');
});

require __DIR__.'/auth.php';
